require("setup.remap")

vim.cmd("set number")
vim.cmd("set relativenumber")
vim.cmd("set autoindent")
vim.cmd("set nobackup")
vim.cmd("set nowritebackup")
